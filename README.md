## Searching images on Flickr

This is a test project for frontend developers.

### Main goals

1. The application can connect to the Flickr API and displays the dumped response.
2. The response is displayed on a user friendly GUI. You have to represent the search results in a collection with the image and some more information of your choice
3. User can type a search keyword and the results are filtered accordingly
4. User can also choose from a pre-defined keyword structure described in a config file.

### Future goals

1. Create a landing page (implemented)
2. Add some graphical feedback (loader) to the user, when the API is searching.  

### Third party components

1. [AngularJS](https://github.com/angular/angular.js)
2. [AngularUI Router](https://github.com/angular-ui/ui-router)
3. [Bootstrap for Sass](https://github.com/twbs/bootstrap-sass)
4. [Font Awesome](https://github.com/FortAwesome/Font-Awesome/)

### Install project

1. Clone this repository
2. Open a command line tool (e.g. Git Bash) and run the following commands: `$ bower install` and `$ npm install`
3. Run `$ gulp` for local development
4. (If you want to publish your work run `$ gulp build`. You have to copy your local json files to the right folder manually.)

#### Notes

- This workflow has some prequisits: Phyton 2.7, npm, bower, gulp, gulp-cli
- If you have some trouble with anything please contact me.

#### Known issues
- WIN7/8/10: MSBUILD error: you might have to install Visual Studio including the C SDK and .NET (2 & 3))
- `$ npm install` failed: make sure that you have Python 2.7 installed and added to your path. Run `$ npm cache clean` after that run `$ npm install` again.