(function() {
    'use strict';

    angular
        .module('flickr.component.flickrHeader.directive', [])
        .directive("flickrHeader", [
        function() {
            return {
                restrict: 'E',
                replace: true,
                scope: false,
                templateUrl: 'app/component/flickrHeader/flickrHeaderTemplate.html',
                compile: function() {
                    return {
                        pre: function( $scope, $element, $attrs ) {},
                        post: function( $scope, $element, $attrs ){}
                    }
                }
            };
        }
    ]);

})();



