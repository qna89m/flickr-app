(function() {
    'use strict';

    angular
        .module('flickr.component.flickrPicture.directive', [])
        .directive("flickrPicture", [
        '$window',
        function($window) {
            return {
                restrict: 'E',
                replace: true,
                scope: "=",
                templateUrl: 'app/component/flickrPicture/flickrPictureTemplate.html',
                compile: function() {
                    return {
                        pre: function( $scope, $element, $attrs ) {
                            $scope.photo.background = $scope.photo.url_z || $scope.photo.url_l || $scope.photo.url_o;
                            $scope.photo.download = $scope.photo.url_o || $scope.photo.url_l || $scope.photo.url_z;
                            $scope.photo.size = {};
                            if ($scope.photo.url_z) {
                                $scope.photo.size = {
                                    width: $scope.photo.width_z,
                                    height: $scope.photo.height_z
                                };
                            }
                            else if ($scope.photo.url_l) {
                                $scope.photo.size = {
                                    width: $scope.photo.width_l,
                                    height: $scope.photo.height_l
                                };
                            }
                            else if ($scope.photo.url_o) {
                                $scope.photo.size = {
                                    width: $scope.photo.width_o,
                                    height: $scope.photo.height_o
                                };
                            }
                            $scope.photo.orientation = $scope.photo.size.height > $scope.photo.size.width ? 'portrait' : 'landscape';
                        },
                        post: function( $scope, $element, $attrs ) {}
                    }
                }
            };
        }
    ]);

})();



