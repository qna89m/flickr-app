(function() {
    'use strict';
    angular
        .module( 'flickr.router', [
            'flickr.pages.landing.controller',
            'flickr.pages.photos.controller'
        ])
        .config( [ '$stateProvider', '$urlRouterProvider',
            function( $stateProvider, $urlRouterProvider ) {

                $stateProvider
                    .state('landing', {
                        url: '/',
                        controller: 'landingController as landingCtrl',
                        templateUrl: 'app/pages/landing/landingView.html'
                    })
                    .state('photos', {
                        url: '/photos',
                        controller: 'photosController as photosCtrl',
                        templateUrl: 'app/pages/photos/photosView.html',
                        params : {
                            searchParams : null,
                            photos : null,
                            tagList : null
                        }
                    });
                $urlRouterProvider.otherwise('/');
            }])
        .run([ '$rootScope', '$state',
            function ($rootScope, $state) {
                 $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
                     switch (toState.name) {
                         case 'photos':
                             if (!toParams.photos){
                                 event.preventDefault();
                                 $state.go('landing');
                             }
                             break;
                         default: break;
                     }

                 });
        }]);
})();