(function() {
    'use strict';
    angular.module('templates', []);
    angular
        .module(
            'flickr-app',
            [
                'templates',
                'ui.router',
                'flickr.router',
                'flickr.api.photo.service',
                'flickr.component.flickrHeader.directive'
            ]
        );
})();