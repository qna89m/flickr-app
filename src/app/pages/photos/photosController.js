(function() {
    'use strict';
    angular
        .module('flickr.pages.photos.controller', [
            'flickr.component.flickrPicture.directive'
        ])
        .controller('photosController', [
            '$scope', '$window', 'PhotoService', '$state', '$stateParams',
            function ($scope, $window, PhotoService, $state, $stateParams) {
                $scope.photos = $stateParams.photos.photo;
                $scope.sidebarOpened = $window.outerWidth>768;
                $scope.searchParams = {
                    keyword : $stateParams.searchParams.keyword,
                    tags : $stateParams.searchParams.tags
                };

                $scope.tagList = $stateParams.tagList;
                $scope.pagination = {
                    page : $stateParams.photos.page,
                    pages : $stateParams.photos.pages
                };

                var tags = {
                    add : function(value){
                        $scope.searchParams.tags.push(value);
                    },
                    remove : function(value){
                        var index = $scope.searchParams.tags.indexOf(value);
                        $scope.searchParams.tags.splice(index, 1);
                    }
                };

                $scope.tagChanged = function(item){
                    if (item.checked) { tags.add(item.tag); }
                    else { tags.remove(item.tag); }
                };

                var _searchParams = angular.copy($scope.searchParams);

                var doSearch = function(newSearch, page){
                    if (newSearch) {
                        _searchParams = angular.copy($scope.searchParams);
                    }
                    if (_searchParams.keyword.length>0 || _searchParams.tags.length>0) {
                        var _response = PhotoService.getPhotos({
                            text: _searchParams.keyword,
                            tags: _searchParams.tags.toString(),
                            page: page
                        });
                        _response.success(function(response) {
                            if (response.stat == "ok") {
                                $scope.photos = response.photos.photo;
                                $scope.pagination = {
                                    page : response.photos.page,
                                    pages : response.photos.pages
                                };
                                $('html, body').animate({
                                    scrollTop: 0
                                }, 800);
                            }
                            //else { console.warn(response); }
                        });
                    }
                };

                $scope.search = function(){
                    doSearch(true, 1);
                };

                $scope.changePage = function(direction){
                    switch (direction){
                        case 'prev':
                            doSearch(false, $scope.pagination.page-1);
                            break;
                        case 'next':
                            doSearch(false, $scope.pagination.page+1);
                            break;
                        default:
                            //console.warn("Undefined direction");
                            break;
                    }
                };
            }
        ]);
})();