(function() {
    'use strict';
    angular
        .module('flickr.pages.landing.controller', [])
        .controller('landingController', [
            '$scope', '$window', 'PhotoService', '$state',
            function ($scope, $window, PhotoService, $state) {

                $scope.searchParams = {
                    keyword : "",
                    tags : []
                };
                $scope.tagList = [];
                var _tagList = PhotoService.getTagList();
                _tagList.success(function(response) {
                    $scope.tagList = response;
                });

                var doSearch = function(){
                    if ($scope.searchParams.keyword.length>0 || $scope.searchParams.tags.length>0) {
                        var _response = PhotoService.getPhotos({
                            text: $scope.searchParams.keyword,
                            tags: $scope.searchParams.tags.toString()
                        });
                        _response.success(function(response) {
                            if (response.stat == "ok") {
                                $state.go('photos', {
                                    searchParams:$scope.searchParams,
                                    photos : response.photos,
                                    tagList: $scope.tagList
                                });
                            }
                            //else { console.warn(response); }
                        });
                    }
                };

                $scope.search = function(){
                    doSearch();
                };

                var tags = {
                    add : function(value){
                        $scope.searchParams.tags.push(value);
                    },
                    remove : function(value){
                        var index = $scope.searchParams.tags.indexOf(value);
                        $scope.searchParams.tags.splice(index, 1);
                    }
                };

                $scope.tagChanged = function(item){
                    if (item.checked) { tags.add(item.tag); }
                    else { tags.remove(item.tag); }
                };
            }
        ]);
})();