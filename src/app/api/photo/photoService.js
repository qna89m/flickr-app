(function() {
    'use strict';

    angular
        .module('flickr.api.photo.service', [])
        .config( [ '$httpProvider', function( $httpProvider ) {}])
        .constant("FlickrAccessPoint", {
            "url" : "https://api.flickr.com/services/rest/"
        })
        .constant('FlickrApiParams', {
            "api_key" : "57f694132e4714c29a64c9af890b124e",
            "format": "json",
            "nojsoncallback": "1",
            "per_page": "60",
            "tag_mode": "all",
            "media" : "photos"
        })
        .service('PhotoService', [
            '$http', 'FlickrAccessPoint', 'FlickrApiParams',
            function ( $http, FlickrAccessPoint, FlickrApiParams ) {
                return {
                    getTagList : function(){
                        return $http({
                            method:"GET",
                            url: 'json/tagList.json'
                        });
                    },
                    getPhotos : function(searchParams){
                        var _extendParams = {
                            method: 'flickr.photos.search',
                            extras : "views, o_dims, url_z, url_l, url_o"
                        };
                        angular.extend(searchParams, FlickrApiParams, _extendParams);
                        return $http({
                            method:"GET",
                            url: FlickrAccessPoint.url,
                            params: searchParams,
                            paramSerializer: '$httpParamSerializerJQLike'
                        });
                    }
                };
            }
        ]);
})();