module.exports = function( gulp, plugin, config ) {

    return function() {
        var watchGlob = plugin.extend( false, [], config.glob.src );
        watchGlob.push( config.path.bower );

        return plugin.watch( 
            watchGlob, 
            function( e ) {

                var reloadDelay = 0;
                if ( e.event == 'add' || e.event == 'unlink' ) {
                    config.task.inject();
                    reloadDelay = 200;
                }

                switch( e.extname ) {
                    case '.scss':
                        config.task.sass();
                        break;
                    case '.json':
                        config.task.wiredep();
                        break;
                    case '.js': 
                        gulp.start('test');
                    case '.html': 
                        setTimeout( function() {
                            plugin.browserSync.reload(); 
                        }, reloadDelay );
                        break;
                    default: break;
                }
            } 
        );
    };
};